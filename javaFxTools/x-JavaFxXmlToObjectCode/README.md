JavaFxXmlToObjectCode javaFxFxml转换代码

根据.fxml文件生成相应的java代码，可生成插件模版

#### 版本记录
- 0.0.1
  1. 完成基本功能配置(根据.fxml文件生成相应的java代码)
- 0.0.2  2020-04-19
  1. 添加生成插件模版功能
